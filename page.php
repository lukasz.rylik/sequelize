<?php
/**
 * Default page
 *
 * @package Sequelize Main
 */

get_header();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		//get_template_part( 'partials/featured-image' );
		?>
	<section id="default">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?php
					the_title( '<h1>', '</h1>' );
					the_content();
					?>
				</div>
			</div>
		</div>
	</section>
		<?php
	}
}
get_footer();
