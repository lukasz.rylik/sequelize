<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package Sequelize Main
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="site-header dark">
    <div class="container">
		<nav class="navbar navbar-expand-md navbar-dark p-2 p-md-0">
			<a class="navbar-brand mx-auto ml-md-0 mr-1" href="<?php echo( get_home_url() ); ?>">
				<?php if ( has_custom_logo() ) {
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo           = wp_get_attachment_image_src( $custom_logo_id, 'full' );
					?>
					<img src="<?php echo esc_url( $logo[0] );?> " class="site-logo" alt="<?php echo get_bloginfo( 'name' );?>">
				<?php } else { ?>
					<h1><?php echo get_bloginfo( 'name' );?></h1>
				<?php } ?>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fas fa-align-left"></span>
			</button>

			<?php
				wp_nav_menu( [
					'menu' => 'primary',
					'depth' => 0,
					'menu_class' => 'navbar-nav w-100 justify-content-end',
					'menu_id' => 'menu-main-top',
					'container' => 'div',
					'container_class' => 'collapse navbar-collapse justify-content-end',
					'container_id' => 'navbarNav',
				] );
			?>
		</nav>
    </div>
</header>

