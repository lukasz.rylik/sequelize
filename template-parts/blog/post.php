<?php
$post_thumbnail_url = ( has_post_thumbnail() ) ? get_the_post_thumbnail_url( $post->ID, 'large'  ) : '';
if ( $url = $post_thumbnail_url ) $backgrounds[] = 'url(' . esc_url( $url ) . ')';

$overlay = get_query_var( 'overlay' );
if ( $url = $overlay ) $backgrounds[] = 'url(' . esc_url( $url ) . ')';

if  ( is_array( $backgrounds ) ) {
	$backgrounds = implode( ',', $backgrounds );
	$inline_css = ' style="background-image: ' . $backgrounds . ';"';
} else {
	$inline_css = '';
}

?>
<div class="col px-0 px-md-3 my-1">
    <article class="px-2 px-md-4 pt-2 pt-md-4" <?php echo( $inline_css ); ?> >
        <?php the_title( '<h3 class="d-block">', '</h3>' ); ?>
        <small class="postdate text-left d-block"><?php echo esc_html( get_the_date( 'j F Y' ) );?></small>
        <span class="text-left d-block">
            <a class="btn btn-white btn btn-light mt-3 py-3 py-md-3 px-3 px-md-5" href="<?php echo ( get_permalink( $post->ID ) ); ?>"><?php _e( 'Read this article','seq' );?></a>
        </span>
    </article>
</div>
