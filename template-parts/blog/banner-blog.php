<?php
$header_text = get_field( 'blog_header_text', get_option( 'page_for_posts' ) );
$inline_css = ( ! empty( $url = get_field( 'blog_header_background_img_url', get_option( 'page_for_posts' ) ) ) ) ? 'url(' . esc_url( $url ) . ')' : '';
if ( $inline_css ) $inline_css = ' style="background-image: ' . $inline_css . ';"';

$intro = get_field( 'blog_intro_text', get_option( 'page_for_posts' ) );
?>
<section id="blog-banner" <?php echo $inline_css; ?> >
    <div class="container">
        <div id="blog-header" class="row">
            <div class="col col-12 col-xs-12">
            <?php if ( ! empty( $header_text ) ) { ?>
                <h2><?php echo esc_html( $header_text );?></h2>
            <?php } ?>
            <?php if ( ! empty ( $intro ) ) { ?>
                <span><?php echo wp_kses_post( $intro ); ?></span>
            <?php } ?>
            </div>
        </div>
</section>
