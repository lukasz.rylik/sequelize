<?php
/**
 * Frontpage Block Main - horizontal slider
 *
 * @package Sequelize Main
 */
if ( $url = get_field( 'fp_main_background_secondary_image' ) ) $backgrounds[] = 'url(' . esc_url( $url ) . ')';
if ( $url = get_field( 'fp_main_background_image' ) ) $backgrounds[] = 'url(' . esc_url( $url ) . ')';


if  ( is_array( $backgrounds ) ) {
	$backgrounds = implode( ',', $backgrounds );
	$inline_css = ' style="background-image: ' . $backgrounds . ';"';
} else {
	$inline_css = '';
}
$inline_css_alt = ''; // feature ... obsolete


$slides = get_field( 'fp_main_slider' );

?>

<?php if ( is_array( $slides ) && count( $slides ) ) { ?>
<section id="mainbody" class="py-5" <?= $inline_css_alt ;?> >
	<div class="widebg" <?= $inline_css ;?> ></div>
	<div class="container">
		<div class="row">
			<div class="col col-12">
				<div id="fp-main-swiper">
					<div class="swiper-wrapper">
						<?php foreach ( $slides as $slide ) {
							set_query_var( 'slide', $slide );
							get_template_part( 'template-parts/front-page/horizontal-slider/h-slide' );
						} ?>
					</div>
					<div class="swiper-button-next swiper-button-white"></div>
					<div class="swiper-button-prev swiper-button-white"></div>
				  </div>
			</div>
		</div>
	</div>
</section>
<?php } ?>
