<?php
/**
 * Frontpage Block Bottom
 *
 * @package Sequelize Main
 */
$title = get_field( 'fp_bottom_title' );
$content = get_field( 'fp_bottom_content' );
$button_text = get_field( 'fp_bottom_button_text' );
$button_url = get_field( 'fp_bottom_button_url' );
$image_url = get_field( 'fp_bottom_image_url' );
if ( ! empty ( $content )
    && ! empty( $title )
) {
?>
<section id="bottom" class="py-5">
	<div class="container">
		<div class="row">
			<div class="col col-12 col-md-7 my-3 my-md-4">
				<article>
				<?php echo ( ! empty( $title ) ) ? '<h2>' . esc_html( $title ) . '</h2>' : ''; ?>
				<?php echo ( ! empty( $content ) ) ? '<span>' . wp_kses_post( $content ) . '</span>' : ''; ?>
				<?php echo ( ! empty( $button_text ) &&  ! empty( $button_url ) ) ? '<a class="btn btn-dark my-3 py-4 py-md-3 px-3 px-md-5" href="' . esc_url( home_url() . $button_url ) . '">' . esc_html( $button_text ) . '</a>' : ''; ?>
				</article>
			</div>
			<?php if( !empty( $image_url ) ) { ?>
			<div class="col col-12 col-md-5 my-3 my-md-4">
				<img class="mx-auto px-5" src="<?= esc_url( $image_url ); ?>" alt=""/>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php }

