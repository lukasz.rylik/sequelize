<?php
/**
 * Frontpage Block Top
 *
 * @package Sequelize Main
 */
$title = get_field( 'fp_top_title' );
$content = get_field( 'fp_top_content' );
$button_text = get_field( 'fp_top_button_text' );
$button_url = get_field( 'fp_top_button_url' );
$image_url = get_field( 'fp_top_image_url' );
?>
<div class="scroll">
	<div class="container">
		<div class="row no-gutters align-items-center">
			<div class="col mx-auto text-center">
					<a class="btn btn-orange p-2 m-1 border-thick rounded-circle" href="#top"><i title="Read more" class="fas fa-chevron-down mx-auto mt-1 fa-4x"></i></a>
			</div>
		</div>
	</div>
</div>
<section id="top" class="py-5">
	<div class="container">
		<div class="row">
			<div class="col col-12 col-md-7 my-3 my-md-4">
				<article>
				<?php echo ( ! empty( $title ) ) ? '<h2>' . esc_html( $title ) . '</h2>' : ''; ?>
				<?php echo ( ! empty( $content ) ) ? '<span>' . wp_kses_post( $content ) . '</span>' : ''; ?>
				<?php echo ( ! empty( $button_text ) &&  ! empty( $button_url ) ) ? '<a class="btn btn-orange my-3 py-4 py-md-3 px-3 px-md-5" href="' . esc_url( home_url() . $button_url ) . '">' . esc_html( $button_text ) . '</a>' : ''; ?>
				</article>
			</div>
			<?php if( !empty( $image_url ) ) { ?>
			<div class="col col-12 col-md-5 my-3 my-md-4">
				<img class="mx-auto px-5" src="<?= esc_url( $image_url ); ?>" alt=""/>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
