<?php
/**
 * Frontpage Block Showcase partial for single slide 
 *
 * @package Sequelize Main
 */
$title = $slide['fp_showcase__slide_title'];
$subtitle = $slide['fp_showcase__slide_subtitle'];
$content = $slide['fp_showcase__slide_content'];
$button_text = $slide['fp_showcase__slide_button_text'];
$button_url = $slide['fp_showcase__slide_button_url'];
$allowed_tags = [ 'em' => [], 'i' => [] ];

?>
<div class="swiper-slide py-5">
	<article>
	<?php echo ( ! empty( $title ) ) ? '<h2>' . esc_html( $title ) . '</h2>' : ''; ?>
	<?php echo ( ! empty( $subtitle ) ) ? '<h3>' . wp_kses( $subtitle, $allowed_tags ) . '<strong>.</strong></h3>' : ''; ?>
	<?php echo ( ! empty( $content ) ) ? '<span>' . wp_kses_post( $content ) . '</span>' : ''; ?>
	<?php echo ( ! empty( $button_text ) &&  ! empty( $button_url ) ) ? '<a class="btn btn-light my-3 py-4 py-md-3 px-5" href="' . esc_url( home_url() . $button_url ) . '">' . esc_html( $button_text ) . '</a>' : ''; ?>
	</article>
</div>
