<?php
/**
 * Frontpage Block Showcase - vertical slider
 *
 * @package Sequelize Main
 */
$background_image_url = get_field( 'fp_showcase_background_image' );
$inline_css =  ( ! empty( $background_image_url ) ) ?
	' style="background-image: url(' . esc_url( $background_image_url )
	. '), linear-gradient(to right, rgba(237,119,64,1), rgba(237,119,64,0));"' 
	: '';
$slides = get_field( 'fp_showcase_slider' );
?>

<?php if ( is_array( $slides ) && count( $slides ) ) { ?>
<section id="showcase" class="py-5">
	<div class="widebg" <?= $inline_css ;?> ></div>
	<div class="container">
		<div class="row">
			<div class="col col-12">
				<div id="fp-showcase-swiper">
					<div class="swiper-wrapper">
						<?php foreach ( $slides as $slide ) {
							set_query_var( 'slide', $slide );
							get_template_part( 'template-parts/front-page/showcase/slide' );
						} ?>
					</div>
					<div class="swiper-pagination"></div>
				  </div>
			</div>
		</div>
	</div>
</section>
<?php }

