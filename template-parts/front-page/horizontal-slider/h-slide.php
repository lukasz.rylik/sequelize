<?php
/**
 * Frontpage Block main partial for single horizontal slide 
 *
 * @package Sequelize Main
 */
$title = $slide['fp_main__slide_title'];
$content = $slide['fp_main__slide_content'];
$button_text = $slide['fp_main__slide_button_text'];
$button_url = $slide['fp_main__slide_button_url'];
$image = $slide['fp_main_image'];
$image_url = '';
$image_alt = '';
if ( is_array( $image ) && count ( $image ) ) {
	$image_url = ( isset( $image[ 'url' ] ) ) ? $image[ 'url' ] : '';
	$image_alt = ( isset( $image[ 'alt' ] ) ) ? $image[ 'alt' ] : '';
}

?>
<div class="swiper-slide py-5">
	<div class="container">
		<div class="row flex-column-reverse flex-md-row">
			<div class="col col-12 col-md-5 pl-md-5 my-3 my-md-5">
				<?= ( ! empty( $image_url ) ) ? '<img class="mx-auto px-5" src="' . $image_url . '" alt="' . $image_alt . '">' : '' ?>
			</div>
			<div class="col col-12 col-md-7 pr-md-5 my-3 my-md-5">
				<article>
				<?php echo ( ! empty( $title ) ) ? '<h2>' . esc_html( $title ) . '</h2>' : ''; ?>
				<?php echo ( ! empty( $content ) ) ? '<span>' . wp_kses_post( $content ) . '</span>' : ''; ?>
				<?php echo ( ! empty( $button_text ) &&  ! empty( $button_url ) ) ? '<a class="btn btn-light my-3 py-4 py-md-3 px-5" href="' . esc_url( home_url() . $button_url ) . '">' . esc_html( $button_text ) . '</a>' : ''; ?>
				</article>
			</div>
		</div>
	</div>
</div>
