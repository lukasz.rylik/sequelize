<?php
/**
 * Blog
 *
 * @package Sequelize Main
 */

get_header();
$overlay = get_field( 'blog_thumbnail_overlay_img', get_option( 'page_for_posts' ) );
set_query_var( 'overlay', $overlay  );

?>
<div id="content">
	<?php get_template_part( 'template-parts/blog/banner','blog' );?>

<?php if ( have_posts() ) { ?>
	<section id="blog" class="blog-columns py-2 py-md-5">
		<div class="container">
			<div id="postcontainer" class="row">
				<?php while ( have_posts() ) {
					the_post();
					get_template_part( 'template-parts/blog/post' );
				} ?>
			</div>
			<?php if (  $wp_query->max_num_pages > 1 ) { ?>
			<div class="row">
				<div class="col col-12 col-xs-12 text-center">
					<a class="btn btn-secondary py-3 py-md-3 px-3 px-md-5 misha_loadmore"><?php _e('Load more', 'seq'); ?></a>
				</div>
			</div>
			<?php } ?>
		</div>
	</section>
</div>
<?php } 
get_footer();
