<?php
function get_href_friendly_phone_number ( $number ) {
    // trunk zero
    $number = str_replace( '(0)', '', $number );
    // dashes
    $number = str_replace( '-', '', $number );
    // whitespaces
    $number = preg_replace('/\s+/', '', $number);
    // nonbreaking spaces utfencoded
    $number = preg_replace('~\x{00a0}~', '', $number);

    return $number;
}