<?php
if ( function_exists( 'acf_add_options_page' ) ) {

// add parent
$parent = acf_add_options_page( array(
    'page_title' => 'Theme General Settings',
    'menu_title' => 'Theme Settings',
    'redirect'   => 'General Settings',
) );

acf_add_options_sub_page( array(
    'page_title'  => 'General Settings',
    'menu_title'  => 'General',
    'parent_slug' => $parent['menu_slug'],
) );

acf_add_options_sub_page( array(
    'page_title'  => 'Company data',
    'menu_title'  => 'Company data',
    'parent_slug' => $parent['menu_slug'],
) );

acf_add_options_sub_page( array(
    'page_title'  => 'Social media',
    'menu_title'  => 'Social media',
    'parent_slug' => $parent['menu_slug'],
) );

}