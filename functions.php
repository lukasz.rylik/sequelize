<?php
/**
 * Functions and definitions
 *
 * @package Sequelize Main
 */

/** Sets up theme defaults and registers support for various WordPress features. */
function general_setup() {
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( [
		'primary' => __( 'Primary Navigation', 'sequelize-main' ),  
		'secondary' => __('Secondary Navigation', 'sequelize-main' )  
	 ] );
	 show_admin_bar( false );
}
add_action( 'after_setup_theme', 'general_setup' );

require_once( 'inc/add-acf-theme-options.php' );
//require_once( 'inc/replace-core-jquery.php' ); // you've been warned...
require_once( 'inc/disable-emojis.php' );
require_once( 'inc/helpers.php' );

/** Enqueues scripts and styles. */
function seq_scripts() {
	wp_enqueue_style( 'seq-style', get_stylesheet_uri() );

	// bootstrap
	wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/dist/css/bootstrap.min.css' );
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/dist/js/bootstrap.min.js', ['jquery'] );

	// fontawesome
	wp_enqueue_script( 'fontawesome_js', get_template_directory_uri() . '/dist/js/fontawesome-all.min.js', ['jquery'] );

	// old ie
	//wp_enqueue_style( 'seq-ie', get_template_directory_uri() . '/dist/css/ie.css', array( 'seq-style' ) );
	//wp_style_add_data( 'seq-ie', 'conditional', 'lt IE 10' );

	wp_enqueue_script( 'seq-html5', get_template_directory_uri() . '/dist/js/html5.js', [] );
	wp_script_add_data( 'seq-html5', 'conditional', 'lt IE 9' );

	//swiper
	wp_enqueue_script( 'seq-swiper', get_template_directory_uri() . '/dist/js/swiper.js', ['jquery'] );
	wp_enqueue_style( 'swiper_generic_css', get_template_directory_uri() . '/dist/css/swiper.min.css', [] );

	//theme
	wp_enqueue_script( 'seq-app', get_template_directory_uri() . '/dist/js/all.min.js', ['jquery'] );
	wp_enqueue_style( 'seq-theme', get_template_directory_uri() . '/dist/css/theme.css' );

	wp_localize_script( 'seq-app', 'ajax_object', [ 'ajax_url' => admin_url( 'admin-ajax.php' ) ] );
}
add_action( 'wp_enqueue_scripts', 'seq_scripts' );

// Lazy loading more posts
require_once( 'inc/add-misha-loadmore.php' );
require_once( 'inc/add-misha-loadmore-ajax-handler.php' );

function seq_custom_logo_setup() {
	$defaults = array(
		'height'      => 100,
		'width'       => 400,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	);
	add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'seq_custom_logo_setup' );

// searchform in top menu
add_theme_support( 'html5', array( 'search-form' ) );
add_filter( 'wp_nav_menu_items', 'add_search_form_to_menu', 10, 2 );
function add_search_form_to_menu($items, $args) {
	if( !( $args->menu == 'primary' ) )
		return $items;
	return $items . '<li class="nav-item searchicon">' . get_search_form(false) . '</li>';
}
add_filter('wpcf7_autop_or_not', '__return_false');

// remove auto-paragraph for acf per theme -> // for global go wp-config: define ( 'WPCF7_AUTOP', false );
// remove_filter( 'acf_the_content', 'wpautop' );
// add_filter( 'acf_the_content', 'wpautop' , 99);

// add_image_size( 'blog-mini', 390, 245, [ 'center', 'center' ] );
// function new_excerpt_more( $more ) {
// 	global $post;
// 	return '… <a class="btn btn-secondary readmore" href="'. get_permalink( $post->ID ) . '">' . 'Read more' . '</a>';
// }
// add_filter( 'excerpt_more', 'new_excerpt_more' );
