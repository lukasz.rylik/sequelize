<?php
get_header();
$form_shortcode = get_field( 'contact_form_shotcode', 'option' ); // autoescaped?
$bgimage_url = get_field( 'contact_form_bg_image_url', 'option' );
$heading = get_field( 'contact_form_heading', 'option' );
if ( empty ( $heading ) ) $heading = __( 'Contact us', 'seq-main' );
$intro = get_field( 'contact_form_intro', 'option' );


$inline_css = ( ! empty( $bgimage_url ) ) ? ' style="background-image: url(' . esc_url( $bgimage_url ) . ');"' : '';
?>
<div id="content">
    <?php if ( ! empty( $form_shortcode ) ) { ?>
    <section id="contact-us" class="py-0 py-md-5">
        <div class="container">
            <div class="row">
                <div class="col col-12 col-12-xs">
                    <div class="card px-3 px-md-5 py-2 py-md-4"  <?php echo( $inline_css );?> >
                        <h2><?php echo( esc_html( $heading ) );?></h2>
                        <?php if ( ! empty( $intro ) ) { ?>
                            <span><?php echo( wp_kses_post( $intro ) );?></span>
                        <?php } ?>
                        <div>
                        <?php echo do_shortcode( $form_shortcode );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
</div>
<?php
get_footer();
