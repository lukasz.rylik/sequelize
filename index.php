<?php
/**
 * The main template file
 *
 * @package Sequelize Main
 */

get_header(); ?>

die ('oops');
<?php if ( have_posts() ) :

	get_template_part( 'partials/featured-image' );
	if ( is_home() && ! is_front_page() ) : ?>
		<header>
			<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
		</header>
	<?php endif;
	while ( have_posts() ) : the_post();
		the_title();
		the_content();
	endwhile;

	the_posts_pagination( array(
		'prev_text'          => __( 'Previous page', 'twentysixteen' ),
		'next_text'          => __( 'Next page', 'twentysixteen' ),
		'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
	) );
else :
	get_template_part( 'template-parts/content', 'none' );

endif;
?>

<?php get_footer();
