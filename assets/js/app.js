(function ($, Swiper) {
	$(document).ready(function() {
		var fpShowcaseSwiper = new Swiper('#fp-showcase-swiper', {
			direction: 'vertical',
			spaceBetween: 30,
			mousewheel: true,
			loop: true,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				dynamicBullets: true,
				dynamicMainBullets: 1,
			},
		});
	});
})(jQuery, Swiper);

(function ($, hSwiper) {
	$(document).ready(function() {
		var fpShowcaseSwiper = new Swiper('#fp-main-swiper', {
			direction: 'horizontal',
			loop: true,
			spaceBetween: 30,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
		});
	});
})(jQuery, Swiper);

// autoenlarge if theres vertical gap
(function ($) {
	var cachedWidth = $(window).width();
	
	function handleResize() {
		var hGap = $('html').height() - $('#content').height() - $('header').height() - $('footer').height();
		if ( 0 < hGap ) {
			$('#content').css('min-height',$('#content').height() + hGap + 'px');
		}
	}
	
	$(document).ready(function() {
		handleResize();
	});
	
	$(window).resize(function() {
		var newWidth = $(window).width();
		if (newWidth !== cachedWidth) {
			cachedWidth = newWidth;
			
			handleResize();
		}
	});
})(jQuery);

// slow scroll on internal #anchors
(function ($) {
	$(document).ready(function() {
		$('a[href*="#"]')
		// Remove links that don't actually link to anything
		.not('[href="#"]')
		.not('[href="#0"]')
		.click(function(event) {
		// On-page links
		if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {
			// Figure out element to scroll to
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			// Does a scroll target exist?
			if (target.length) {
			// Only prevent default if animation is actually gonna happen
			event.preventDefault();
			$('html, body').animate({
			  scrollTop: target.offset().top
			}, 1000, function() {
			  // Callback after animation
			  // Must change focus!
			  var $target = $(target);
			  $target.focus();
			  if ($target.is(":focus")) { // Checking if the target was focused
				return false;
			  } else {
				$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
				$target.focus(); // Set focus again
			  }
			});
			}
		}
		});
	});
})(jQuery);