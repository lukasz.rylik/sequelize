<?php
/**
 * Blog single post
 *
 * @package Sequelize Main
 */

get_header();
$overlay = get_field( 'blog_thumbnail_overlay_img', get_option( 'page_for_posts' ) );
?>
<div id="content">
<?php if ( have_posts() ) { ?>
	<section id="blog" class="py-5">
		<div class="container">
			<div class="row">
				<div class="col col-12 col-xs-12">
					<article>
							<?php the_post(); ?>
							<small class="postdate"><?php echo esc_html( get_the_date( 'j F Y' ) );?></small>
							<?php the_title( '<h1 class="posttitle">', '</h1>' ); ?>
							<?php
							if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'large' );
							};
							?>
							<?php echo wp_kses_post( wpautop( get_the_content( '', false ) ) ); ?>
					</article>
				</div>
			</div>
		</div>
	</section>
</div>
<?php } 
get_footer();