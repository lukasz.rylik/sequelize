<?php
/**
 * Footer
 *
 * @package Sequelize Main
 */
$company_address_separator = ' ';
if ( $addr_part = esc_html( get_field( 'cmp_addr_street', 'option' ) ) ) $company_address[] = $addr_part;
if ( $addr_part = esc_html( get_field( 'cmp_addr_city', 'option' ) ) ) $company_address[] = $addr_part;
if ( $addr_part = esc_html( get_field( 'cmp_addr_postcode', 'option' ) ) ) $company_address[] = $addr_part;

if ( ! $company_email = sanitize_email( get_field( 'custom_email', 'option' ) ) ) {
	$contact_user = get_field( 'useremail', 'option' );
	if ( isset( $contact_user['user_email'] ) ) {
		$company_email = sanitize_email( $contact_user[ 'user_email' ] );
	}
}
$company_tel = get_field( 'phone_no', 'option' );
// drama, call helper
$company_tel_hrefoptimised = ( ! empty ( $company_tel ) ) ? get_href_friendly_phone_number( $company_tel ) : '';

if ( empty( $company_name = get_field( 'cmp_name', 'option' ) ) ) $company_name = get_bloginfo( 'name' );
if ( empty( $year_from = get_field( 'cmp_copyright_year_from', 'option' ) ) ) $year_from =  date( 'Y' );

$reg_no = get_field( 'cmp_registration_number', 'option' );
$additional_info = get_field( 'cmp_additional_info', 'option' );

?>
<footer class="dark">
	<div class="container py-4 px-4 px-md-0">
		<div class="row">
            <div class="col-xs-12 col-md-4 text-center text-md-left">
				<?php if ( has_custom_logo() ) {
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo           = wp_get_attachment_image_src( $custom_logo_id, 'full' );
					?>
					<a href="<?php echo( get_home_url() ); ?>">
						<img src="<?php echo esc_url( $logo[0] );?> " class="site-logo" alt="<?php echo get_bloginfo( 'name' );?>">
					</a>
				<?php } else { ?>
					<h1>
						<a href="<?php echo( get_home_url() ); ?>">
							<?php echo get_bloginfo( 'name' );?>
						</a>
					</h1>
				<?php } ?>
            </div>
            <div class="col-xs-12 col-md-8 text-uppercase">
				<div id="footer-menu" class="row no-gutters">
					<div class="col col-12">
						<?php wp_nav_menu( [ 'menu' => 'secondary', 'depth' => 0 ] ); ?>
					</div>
				</div>
				<div id="companydata" class="row">
					<div class="col col-12 text-center py-3 py-md-0">
						<?php if ( is_array( $company_address ) ) { ?>
						<span class="d-block d-md-inline" aria-label="<?php echo( _e( 'Address', 'seq' ) ); ?>">
							<?php echo( implode( $company_address_separator, $company_address ) ); ?>
						</span>
						<?php } ?>

						<?php if ( ! empty( $company_tel_hrefoptimised ) ) { ?>
						<span class="d-block d-md-inline text-nowrap">
							<span id="phone-label">
								<?php echo( _e( 'Tel:', 'seq' ) ); ?>
							</span>
							<span>
								<a class="text-white" href="tel:<?php echo( esc_html( $company_tel_hrefoptimised ) ); ?>" aria-labelledby="phone-label">
									<?php echo( esc_html( $company_tel ) ); ?>
								</a>
							</span>
						</span>
						<?php } ?>

						<?php if ( ! empty( $company_email ) ) { ?>
						<span class="d-block d-md-inline text-nowrap">
							<span id="email-label">
								<?php echo( _e( 'Email:', 'seq' ) ); ?>
							</span>
							<span>
								<a class="text-white" href="mailto:<?php echo( esc_html( $company_email ) ); ?>" aria-labelledby="email-label">
									<?php echo( esc_html( $company_email ) ); ?>
								</a>
							</span>
						</span>
						<?php } ?>

					</div>
					<div class="col col-12 text-center">

						<?php if ( ! empty( $year_from ) && ! empty( $company_name ) ) { ?>
						<span class="d-block d-lg-inline">
							Copyright &copy; <?php echo esc_html( $year_from ); ?> <i class="aria-hidden">&sdot;&nbsp;</i> <?php echo esc_html( $company_name );?>
						</span>
						<?php } ?>
						
						<span class="d-block d-lg-inline">
							
							<?php if ( ! empty( $additional_info ) ) { ?>
							<span class="d-block d-md-inline text-nowrap">
								<i aria-hidden="true" class="d-none d-lg-inline">&sdot;&nbsp;</i><?php echo esc_html( $additional_info ); ?>
							</span>
							<?php  } ?>

							<?php if ( ! empty( $reg_no ) ) { ?>
							<span class="d-block d-md-inline text-nowrap">
								<i aria-hidden="true" class="d-none d-md-inline">&nbsp;&sdot;&nbsp;</i> <?php echo( _e( 'Company no.', 'seq' ) ); ?> <?php echo esc_html( $reg_no );?>
							</span>
							<?php } ?>

							</span>
						</span>

					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php
wp_footer();
