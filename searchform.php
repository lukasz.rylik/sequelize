<?php
/**
 * Template for displaying search form in top menu
 *
 * @package Sequelize Main
 */
?>
<form method="get" class="form-inline" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="input-group">
        <input type="text" id="s" class="form-control form-control-sm" aria-label="<?php _e( 'Search input', 'seq' ); ?>" placeholder="<?php esc_attr_e( 'Search', 'seq' ); ?>" />
        <div class="input-group-append">
            <button type="submit" class="btn btn-sm btn-outline-light" name="submit" id="searchsubmit" aria-label="<?php esc_attr_e( 'Search', 'seq' ); ?>">
                    <i title="<?php esc_attr_e( 'Search', 'seq' ); ?>" class="fas fa-search"></i>
            </button>
        </div>
    </div>
</form>