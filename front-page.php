<?php
/**
 * Frontpage
 *
 * @package Sequelize Main
 */

$background_image_url = get_field( 'fp_general_background_image_url' );
$inline_css =  ( ! empty( $background_image_url ) ) ? ' style="background-image: url(' . esc_url( $background_image_url ) . ');"' : '';

get_header();

get_template_part( 'template-parts/front-page/block', 'showcase' );
?>
<div id="content">
<div class="widebg" <?= $inline_css; ?>></div>
<?php
get_template_part( 'template-parts/front-page/block', 'top' );
get_template_part( 'template-parts/front-page/block', 'main' );
get_template_part( 'template-parts/front-page/block', 'bottom' );
?>
</div>
<?php
get_footer();
